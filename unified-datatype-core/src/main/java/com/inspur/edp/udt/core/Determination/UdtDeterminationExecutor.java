/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Determination;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.ValueObjDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
import com.inspur.edp.udt.api.Udt.IUdtContext;
import java.util.Objects;

public class UdtDeterminationExecutor extends ValueObjDeterminationExecutor {

  private final ICefDeterminationContext parentContext;

  public UdtDeterminationExecutor(ICefValueObjContext context, IValueObjRTDtmAssembler ass,
      ICefDeterminationContext parent, ValueObjModifyChangeDetail changedetail) {
    super(context, ass, changedetail);
    parentContext = parent;
  }

  @Override
  protected ICefDeterminationContext createDeterminationContext() {
    return new UdtDeterminationContext((IUdtContext) getNodeContext(), parentContext);
  }
}
