/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem.sqlsnippetprocessor;

import com.inspur.edp.cef.repository.adaptoritem.EntityRelationalReposAdaptor;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessorFactory;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;
import com.inspur.edp.cef.repository.sqlgenerator.ISqlGenerator;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;

public class SqlSnippetProcessor {
    private ISqlGenerator sqlGenerator;
    private String tableAlias;
    private String primaryColumnName;
    private boolean isChild = false;
    private EntityRelationalReposAdaptor adaptor;
    DbProcessor dbProcessor;
    public SqlSnippetProcessor(EntityRelationalReposAdaptor adaptor){
        this.adaptor = adaptor;
        if(!StringUtils.isNullOrEmpty(adaptor.getWrappedOriginTableAlias())){
            this.tableAlias = adaptor.getWrappedOriginTableAlias();
        }
        else {
            this.tableAlias = adaptor.getWrappedTableAlias();
        }
        this.primaryColumnName = adaptor.getContainColumns().getPrimaryKey().getDbColumnName();
        this.isChild = adaptor.getIsChild();
        this.sqlGenerator = SqlGeneratorFactory.getSqlProcessor(adaptor.getGspDbType());
        this.dbProcessor= DbProcessorFactory.getDbProcessor(adaptor.getEntityDac().getGspDbtype());
    }

    public final DbProcessor getDbProcessor()
    {return dbProcessor;}

    public ISqlGenerator getSqlGenerator(){
        return sqlGenerator;
    }

    public String parentJoin(){
        if(isChild){
            String joinSql = getSqlGenerator().getInnerJoinTableName();
            DbColumnInfoCollection columnInfos = adaptor.getBaseAdaptorItem().getContainColumns();
            String parentID = "";
            for (DbColumnInfo columnInfo:columnInfos){
                if(columnInfo.getIsParentId()){
                    parentID = columnInfo.getDbColumnName();
                    break;
                }
            }
            joinSql = joinSql.replace("@ParentID@", this.tableAlias+"." + parentID);
            return joinSql;
        }
        return "";
    }


    public String getQuerySql(){
        String querySql = getSqlGenerator().getQuerySql();
        return querySql;
    }

    public String getDeleteSqlBatch() {
        String deleteSql = getSqlGenerator().getDeleteSql();
        return String.format(deleteSql, "@TableName@", tableAlias);
    }

    public String getGetDataByIdsSql() {
        String retrieveSql = getSqlGenerator().getRetrieveBatchSql();
        return String.format(retrieveSql, "%1$s", "%2$s", tableAlias + "." + primaryColumnName, "%3$s");
    }

    public String getGetDataByIdSql() {
        String retrieveSql = getSqlGenerator().getRetrieveSql();
        return String.format(retrieveSql, "%1$s", "%2$s", tableAlias + "." + primaryColumnName, "%3$s");
    }

    public String getJoinTableName() {
        return getSqlGenerator().getJoinTableName();
    }

    public String innerGetDeleteSql(){
        String deleteSql = getSqlGenerator().getDeleteSql();
        return String.format(deleteSql, "@TableName@", tableAlias); //getTableNameWithYear(tableInfo)
    }

    //insertsql 没啥用了
    public String innerGetInsertSql(){
        return "";
    }

    public String innerGetModifySql(){
        String modifySql = "Update %1$s Set";
        return String.format(modifySql, "@TableName@"); //getTableNameWithYear(tableInfo)
    }
}
