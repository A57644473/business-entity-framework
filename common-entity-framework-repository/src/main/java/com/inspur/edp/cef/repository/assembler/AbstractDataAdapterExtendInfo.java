/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.adaptor.IDataAdapterExtendInfo;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdapterContext;
import com.inspur.edp.cef.api.repository.adaptor.InsertFieldsInfo;
import com.inspur.edp.cef.api.repository.dac.IDataTypeDac;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.repository.dac.ChildEntityDac;
import com.inspur.edp.cef.repository.dac.EntityDac;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractDataAdapterExtendInfo implements IDataAdapterExtendInfo {


    //    private IEntityAdapterContext context;
    private DataAdapterExtendInfoContext context;

    protected AbstractDataAdapterExtendInfo(DataAdapterExtendInfoContext context) {
        this.context = context;
    }

    protected DataAdapterExtendInfoContext getContext() {
        return context;
    }

//    protected IEntityAdapterContext getEntityAdapterContext() {
//        return context;
//    }
//
//    public void setEntityAdapterContext(IEntityAdapterContext value) {
//        context = value;
//    }

//    public abstract String getQueryFields();
//
//    public abstract void setQueryFields(List<String> mappingColumns);

//    public abstract InsertFieldsInfo getInsertFields(IEntityData data);

    public abstract HashMap<String, AbstractDataAdapterExtendInfo> getChildExtendInfos();

    public abstract DbColumnInfo tryGetColumnInfoByPropertyName(String propName);

    public abstract void setEntityPropertiesFromDal(ICefReader reader, IEntityData data);

    public DbColumnInfoCollection getDbColumnInfos() {
        return null;
    }

    public ArrayList<AssociationInfo> getAssociationInfos(){ return null; }

    public abstract void buildModifyValues(StringBuilder modifyValue, Map<String, Object> propertyChanges, RefObject<List<DbParameter>> parameters);

    public abstract IDataTypeDac getCurrentDac(EntityDac parentDac);

    public abstract Map<String, ChildEntityDac> getChildDacList(EntityDac parentDac);

    public abstract Object getUdtInsertValue(String labelId, String belongEleLabelId, IEntityData entityData);
}
