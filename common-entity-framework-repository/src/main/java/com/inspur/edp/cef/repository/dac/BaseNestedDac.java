/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dac;


import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.api.repository.adaptor.INestedAdaptor;
import com.inspur.edp.cef.api.repository.dac.INestedDac;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.repository.adaptor.NestedAdaptor;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessorFactory;
import java.util.HashMap;

//单值关联业务字段在be上识别进行join;
//也有一个dac, 通过readValue赋值;
public abstract class BaseNestedDac implements INestedDac
{
	private INestedRepository privateBelongRepository;
	public final INestedRepository getBelongRepository()
	{
		return privateBelongRepository;
	}
	public final void setBelongRepository(INestedRepository value)
	{
		privateBelongRepository = value;
	}

	public final ICefData readData(ICefReader reader)
	{
		return InnerGetAdapter().readData(reader);
	}
	private INestedAdaptor InnerGetAdapter()
	{
		INestedAdaptor adapter = getAdaptor();
		adapter.setBelongRepository(getBelongRepository());
		return adapter;
	}

	public abstract INestedAdaptor getAdaptor();

	public final void initParams(java.util.Map<String, Object> pars)
	{
		throw new UnsupportedOperationException();
	}

	public final Object getPersistenceValue(String colName, ICefData data)
	{
		return InnerGetAdapter().getPersistenceValue(colName, data);
	}

	public final Object getPersistenceValue(String colName, ICefData data, boolean isNull) {
		return InnerGetAdapter().getPersistenceValue(colName, data, isNull);
	}

	@Override
	public HashMap<String, String> getAssosPropDBMapping(String propName) {
		return ((NestedAdaptor)InnerGetAdapter()).getAssosPropDBMapping(propName);
	}
}
