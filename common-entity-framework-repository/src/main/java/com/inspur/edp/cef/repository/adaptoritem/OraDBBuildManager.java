/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.StandardBasicTypes;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public class OraDBBuildManager {
    private Query buildQueryManager(EntityManager entityManager, String sqlText, List<DbParameter> parameters) {
        DataValidator.checkForEmptyString(sqlText, "sqlText");
        Query query = entityManager.createNativeQuery(sqlText);
        if (parameters != null) {
            for (int i = 0; i < parameters.size(); i++) {
                DbParameter param = parameters.get(i);
                switch(param.getDataType()){
                    case VarChar:
                    case NVarChar:
                    case NChar:
                    case Blob:
                        query.setParameter(i,param.getValue());
                        break;
                    case Clob:
                        //TODO 后续测试DM数据库是否合适
                        query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
                        break;
                    case Int:
                        query.setParameter(i,new TypedParameterValue(StandardBasicTypes.INTEGER, param.getValue()));
                        break;
                    case Decimal:
                        query.setParameter(i,new TypedParameterValue(StandardBasicTypes.BIG_DECIMAL, param.getValue()));
                        break;
                    case Char:
                        if(param.getValue()==null){
                            query.setParameter(i,new TypedParameterValue(StandardBasicTypes.STRING, param.getValue()));

                        }else {
                            query.setParameter(i,param.getValue()) ;
                        }
                        break;
                    case NClob:
                        query.setParameter(i, new TypedParameterValue(StandardBasicTypes.NTEXT, param.getValue()));
                        break;
                    case DateTime:
                        query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                        break;
                    case Date:
                        if(param.getValue() == null){
                            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                            break;
                        }

                        java.sql.Date date = new  java.sql.Date(((Date) param.getValue()).getTime());
                        date = java.sql.Date.valueOf(date.toString());
                        query.setParameter(i, date, TemporalType.DATE);
                        break;
                    default:
                        throw new RuntimeException("未知的数据类型，请确认参数的类型。");
                }
            }
        }
        return query;
    }
}
