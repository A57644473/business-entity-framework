/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor;

import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.readerwriter.CefMappingReader;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleAssoUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleUdtPropertyInfo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleUdtRepoProcessor extends AbstractRepoPrcessor {

  @Override
  public Object readProperty(DbColumnInfo dbColumnInfo, DbProcessor dbProcessor,
      BaseAdaptorItem adaptorItem, ICefReader reader) {
    SimpleUdtPropertyInfo simpleUdtPropertyInfo= (SimpleUdtPropertyInfo) dbColumnInfo.getDataTypePropertyInfo().getObjectInfo();
    HashMap<String,String> propMapping = simpleUdtPropertyInfo.getPropMapping();
    if(propMapping==null)
    {
      propMapping=new LinkedHashMap<>();
      String udtPropName = simpleUdtPropertyInfo.getUdtConfigId();
      if(simpleUdtPropertyInfo.getUdtConfigId().contains("."))
        udtPropName=simpleUdtPropertyInfo.getUdtConfigId().substring(simpleUdtPropertyInfo.getUdtConfigId().lastIndexOf(".")+1);
      propMapping.put(udtPropName,dbColumnInfo.getDataTypePropertyInfo().getPropertyName());
      simpleUdtPropertyInfo.setPropMapping(propMapping);
    }
    CefMappingReader mappingReader = new CefMappingReader(propMapping, reader);

    INestedRepository u1Repos = adaptorItem.getNestedRepo(simpleUdtPropertyInfo.getUdtConfigId());
    return u1Repos.readData(mappingReader);
  }

  @Override
  public Object getPropertyChangeValue(DbColumnInfo dbColumnInfo, Object value) {
    return null;
  }

  @Override
  public Object getPersistenceValue(DbColumnInfo dbColumnInfo, Object value) {
    throw new RuntimeException();
  }
}
