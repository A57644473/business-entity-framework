/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

public abstract class ChildEntityAccessor extends EntityAccessor implements IChildAccessor
{
	protected ChildEntityAccessor(IEntityData data)
	{
		super(data);
	}


	@JsonIgnore
	private IEntityAccessor privateParent;
	@JsonIgnore
	public final IEntityAccessor getParent()
	{
		return privateParent;
	}
	public final void setParent(IEntityAccessor value)
	{
		privateParent = value;
	}



	@Override
	@JsonIgnore
  public IAccessor getRoot() {
	return 	(getParent() != null && ((getParent().getRoot())) != null) ? (getParent().getRoot()) : this;
	}
}
