/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import java.util.ArrayList;
import java.util.List;

public class FieldsFilter {
    private boolean useFieldsCondition=false;

    public boolean isUseFieldsCondition() {
        return useFieldsCondition;
    }

    public void setUseFieldsCondition(boolean useFieldsCondition) {
        this.useFieldsCondition = useFieldsCondition;
    }

    private List<String> filterFields = new ArrayList<>();

    public List<String> getFilterFields() {
        return filterFields;
    }

    public FieldsFilter(boolean useFieldsCondition) {
        this.useFieldsCondition = useFieldsCondition;
    }

    public FieldsFilter ()
    {}

    public static FieldsFilter mergeFieldsFilter(FieldsFilter source,FieldsFilter target)
    {
        if(source==null)
            return target;
        if(target==null)
            return source;
        if(source.isUseFieldsCondition()==false) {
            target.setUseFieldsCondition(false);
        return target;}
        if(target.isUseFieldsCondition()==false)
            return target;
        for(String field:source.filterFields)
        {
            if(target.filterFields.contains(field))
                continue;
            target.filterFields.add(field);
        }
        return target;
    }

}
