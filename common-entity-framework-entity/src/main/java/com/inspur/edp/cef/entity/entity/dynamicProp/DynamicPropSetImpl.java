/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity.dynamicProp;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

  @JsonDeserialize(using = DynamicPropSetJsonDeserializer.class)
  @JsonSerialize(using = DynamicPropSetJsonSerializer.class)
public class DynamicPropSetImpl implements IDynamicPropSet {
  private java.util.HashMap<String, Object> _props;

  public DynamicPropSetImpl() {
  }

  protected java.util.Map<String, Object> getProps() {
    return (_props != null) ? _props : (_props = new java.util.HashMap<String, Object>());
  }

  public ICefData copySelf() {
    DynamicPropSetImpl tempVar = new DynamicPropSetImpl();
    tempVar._props = new java.util.HashMap<String, Object>(getProps());
    return tempVar;
  }

  public ArrayList<String> getPropertyNames() {

    ArrayList<String> list = new ArrayList<>();
    for (Map.Entry<String, Object> entry : getProps().entrySet()) {
      list.add(entry.getKey());
    }
    return list;
  }

  public final Object getValue(String propName) {
    DataValidator.checkForNullReference(propName, "propName");
    for (Map.Entry<String, Object> entry : getProps().entrySet()) {
      if (entry.getKey().equals(propName)) {
        return entry.getValue();
      }
    }
    throw new RuntimeException("invalid propName:" + propName);
  }

  public final void setValue(String propName, Object value) {
    DataValidator.checkForNullReference(propName, "propName");

    getProps().put(propName, value);
  }

  public void clear() {
    getProps().clear();
  }

  public final ICefData copy() {
    DynamicPropSetImpl tempVar = new DynamicPropSetImpl();
    tempVar._props = new java.util.HashMap<String, Object>(getProps());
    return tempVar;
  }

  @Override
  public boolean contains(String propName){
    Objects.requireNonNull(propName);

    if (getProps().containsKey(propName)) {
      return true;
    }
    return false;
  }

  @Override
  public Iterator<IDynamicPropPair> iterator() {
    return getProps().entrySet().stream()
        .map(entry -> (IDynamicPropPair) new DynamicPropPair(entry.getKey(), entry.getValue()))
        .iterator();
  }
  //endregion
}
