/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class MgrConfigDeserializer extends JsonDeserializer<MgrConfig> {
    public MgrConfig deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        MgrConfig mgrConfig=new MgrConfig();
        SerializerUtils.readStartObject(jsonParser);

        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(mgrConfig, propName, jsonParser);
        }

        SerializerUtils.readEndObject(jsonParser);
        return  mgrConfig;
    }

    private void readPropertyValue(MgrConfig mgrConfig, String propName, JsonParser jsonParser) {
        switch (propName)
        {
            case "impClass":
                mgrConfig.setImpClass(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case "className":
                mgrConfig.setClass(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case "allInterfaceClassName":
                mgrConfig.setAllInterfaceClassName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case "allInterfaceAssembly":
                mgrConfig.setAllInterfaceAssembly(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case "assembly":
                mgrConfig.setAllInterfaceAssembly(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case "impAssembly":
                mgrConfig.setAllInterfaceAssembly(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                throw new RuntimeException("Unrecognized Property Name:"+propName);
        }
    }
}
