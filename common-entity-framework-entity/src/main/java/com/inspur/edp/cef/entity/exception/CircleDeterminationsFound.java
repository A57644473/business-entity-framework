/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.exception;


public class CircleDeterminationsFound extends RuntimeException
{
	//demo暂不考虑国际化
	private static final String ErrorCode = "Cef";
	private static final String ErrorMessage = "发现Determination环, 执行顺序如下:";

	public CircleDeterminationsFound(String history)
	{
//		super(ErrorCode, ErrorMessage + history, null, ExceptionLevel.Fatal, false);
	}

//	protected CircleDeterminationsFound(SerializationInfo info, StreamingContext context)
//	{
//		super(info, context);
//	}
}
