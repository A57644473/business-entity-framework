package com.inspur.edp.cef.variable.core.manager;


import com.inspur.edp.cef.api.manager.ICefDataTypeManager;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.manager.action.IMgrActionExecutor;
import com.inspur.edp.cef.spi.manager.MgrActionExecutor;
import com.inspur.edp.cef.variable.api.manager.IVariableManager;
import com.inspur.edp.cef.variable.api.manager.IVariableMgrContext;

public final class VarMgrContext implements IVariableMgrContext
{
	public VarMgrContext()
	{
	}

	private IVariableManager privateVarManager;
	public IVariableManager getVarManager()
	{
		return privateVarManager;
	}
	public void setVarManager(IVariableManager value)
	{
		privateVarManager = value;
	}


	 public void setManager(ICefDataTypeManager value){
		privateVarManager= (IVariableManager) value;
	 }

	public <T> IMgrActionExecutor<T> getActionExecutor()
	{
		return new MgrActionExecutor<T>();
	}

	public ICefValueObjManager getManager() {
		return privateVarManager;
	}
}