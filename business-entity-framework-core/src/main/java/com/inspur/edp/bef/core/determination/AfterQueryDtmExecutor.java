/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.spi.event.queryactionevent.BefQueryEventBroker;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

import java.util.List;

public class AfterQueryDtmExecutor extends EntityDeterminationExecutor {

    // region Ctor

    private IBENodeEntityContext innerGetEntityContext() {
        return (IBENodeEntityContext) super.getEntityContext();
    }

    // private readonly IBEContext entityCtx;
    public AfterQueryDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx) {
        super(assembler, entityCtx);
    }

    private IBEManager beManager;

    public AfterQueryDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx, IBEManager beManager) {
        super(assembler, entityCtx);
        this.beManager = beManager;
        //this.entityCtx = entityCtx;
    }
    // endregion

    // region Execute
    @Override
    public void execute() {
        List<IDetermination> dtms = getAssembler().getDeterminations();
        int count = 0;
        if (dtms == null) {
            return;
        }

        ICefDeterminationContext context = this.GetContext();
        try {
            BefQueryEventBroker.fireBeforeDataCXHDeterminate(beManager, context);
            for (IDetermination dtm : dtms) {
                if (!dtm.canExecute(null)) {
                    continue;
                }
                count++;
                try {
                     BefQueryEventBroker.firebeforeCXHDeterminate(beManager,context);
                    dtm.execute(context, null);
                     BefQueryEventBroker.fireafterCXHDeterminate(beManager,context);
                } catch (Exception e) {
                    BefQueryEventBroker.fireQueryUnnormalStop(e);
                    throw new RuntimeException(e);
                }
            }
            BefQueryEventBroker.fireAfterDataCXHDeterminate(beManager, context, count);
        } catch (Exception e) {
            BefQueryEventBroker.fireQueryUnnormalStop(e);
            throw new RuntimeException(e);
        }
    }

    // endregion
}
