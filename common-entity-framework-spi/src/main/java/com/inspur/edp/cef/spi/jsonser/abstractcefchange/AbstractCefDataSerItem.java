

package com.inspur.edp.cef.spi.jsonser.abstractcefchange;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.api.manager.serialize.NestedSerializeContext;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.CamelConst;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropPair;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import com.inspur.edp.cef.spi.common.UdtManagerUtil;
import com.inspur.edp.cef.spi.jsonser.base.IDynamicPropSerItem;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import io.iec.edp.caf.common.JSONSerializer;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public abstract class AbstractCefDataSerItem {

  public abstract void writeEntityBasicInfo(JsonGenerator jsonGenerator, ICefData data,
      SerializerProvider serializerProvider);

  public abstract boolean writeModifyPropertyJson(JsonGenerator jsonGenerator, String propertyName,
      Object value,
      SerializerProvider serializerProvider);
  // 产品部不用devserver如果有以前生成的dll启动服务会报错找不到这个方法, 先留着产品部全部重新生成之后在删掉;

  // 2018年12月7日注意, 以下write方法不走camel, 为兼容已生成代码暂时不能去掉.
  // 将来提取到新类中可以改成camel;

  /// #region Write

  /**
   * 将枚举类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeEnum(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeEnum(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将字符类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeString(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeString(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将整数类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeInt(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeInt(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将小数类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeDecimal(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeDecimal(jsonGenerator, value, propertyName, serializerProvider,getCefSerializeContext().isBigNumber());
  }

  protected final void writeDecimal(JsonGenerator jsonGenerator, Object value, String propertyName,
                                    SerializerProvider serializerProvider, boolean isBigNumber) {
    SerializerUtil.writeDecimal(jsonGenerator, value, propertyName, serializerProvider, isBigNumber);
  }

  /**
   * 将布尔类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeBool(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeBool(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将日期类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeDateTime(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeDateTime(jsonGenerator, value, propertyName, serializerProvider,
        getCefSerializeContext().getEnableStdTimeFormat());
  }

  private void writeDateTime(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider, Boolean enableStdFormat) {
    try {
      jsonGenerator.writeFieldName(StringUtils.toCamelCase(propertyName));
      if (value == null) {
        jsonGenerator.writeNull();
      } else {
        if (enableStdFormat) {
          jsonGenerator.writeString(BefDateSerUtil.getInstance().writeStdDateTime((Date) value));
        } else {
          jsonGenerator.writeString(BefDateSerUtil.getInstance().writeDateTime((Date) value));
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 写入Json对象属性名
   * @param jsonGenerator
   * @param propertyName
   */
  protected final void writeFieldName(JsonGenerator jsonGenerator,
      String propertyName) {
    try {
      jsonGenerator.writeFieldName(propertyName);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  /**
   * 写入Json对象开头
   * @param jsonGenerator
   */
  protected final void writeStartObject(JsonGenerator jsonGenerator) {
    try {
      jsonGenerator.writeStartObject();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  /**
   * 写入Json对象属性名
   * @param jsonGenerator
   */
  protected final void writeEndObject(JsonGenerator jsonGenerator) {
    try {
      jsonGenerator.writeEndObject();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected final void writeStdDateTime(JsonGenerator jsonGenerator, Object value,
      String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeDateTime(jsonGenerator, value, propertyName, serializerProvider, true);
  }

  protected final void writeDate(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeDate(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将二进制数组类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeBytes(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    SerializerUtil.writeBytes(jsonGenerator, value, propertyName, serializerProvider);
  }

  /**
   * 将关联类型数据写入到Json中
   *
   * @param jsonGenerator Json写入器
   * @param value 要写入的值
   * @param propertyName 要写入的属性名称
   * @param serializerProvider Json序列化类
   */
  protected final void writeAssociation(JsonGenerator jsonGenerator, Object value,
      String propertyName,
      SerializerProvider serializerProvider) {
    writeBaseType(jsonGenerator, value, propertyName, serializerProvider);
  }

  protected final void writeBaseType(JsonGenerator jsonGenerator, Object value, String propertyName,
      SerializerProvider serializerProvider) {
    try {
      jsonGenerator.writeFieldName(StringUtils.toCamelCase(propertyName));
      ObjectMapper mapper = new ObjectMapper();
      String valueTemp = mapper.writeValueAsString(value);
      jsonGenerator.writeRawValue(valueTemp);

      // serializerProvider.defaultSerializeValue(value, jsonGenerator);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected final void writeNestedValue(JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider,
      String nestedConfigId, ICefData value, String propertyName, NestedSerializeContext context) {
    ICefValueObjManager val = (ICefValueObjManager) UdtManagerUtil.getUdtFactory()
        .createUdtManager(nestedConfigId);
    val.setNestedSerializeContext(context);
    String str = val.serialize(value);
    try {
      jsonGenerator.writeFieldName(StringUtils.toCamelCase(propertyName));
      jsonGenerator.writeRawValue(str);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    // Java版临时屏蔽UDT
    // Object tempVar =
    // UdtManagerUtil.getUdtFactory().CreateManager(nestedConfigId);
    //// C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing
    // in Java:
    // var val = (ICefValueObjManager)((tempVar instanceof ICefValueObjManager) ?
    // tempVar : null);

    // var serializedValue = val.Serialize(value);
    // //TODO: udtData序列化结果改成json对象形式
    // writer.WritePropertyName(StringUtils.toCamelCase(propertyName));
    // writer.WriteRawValue(serializedValue);
    // //writeBaseType(writer, JsonConvert.DeserializeObject(serializedValue),
    // propertyName, serializer);
  }

  protected final void writeNestedValue(JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider,
      String nestedConfigId, ICefData value, String propertyName) {
    writeNestedValue(jsonGenerator, serializerProvider, nestedConfigId, value, propertyName, null);
  }

  protected final void writeNestedChange(JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider,
      String nestedConfigId, IChangeDetail value, String propertyName,
      NestedSerializeContext context) {
    ICefValueObjManager val = (ICefValueObjManager) UdtManagerUtil.getUdtFactory()
        .createUdtManager(nestedConfigId);
    val.setNestedSerializeContext(context);
    String str = val.serializeChange(value);
    try {
      jsonGenerator.writeFieldName(StringUtils.toCamelCase(propertyName));
      jsonGenerator.writeRawValue(str);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected final void writeNestedChange(JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider,
      String nestedConfigId, IChangeDetail value, String propertyName) {
    writeNestedChange(jsonGenerator, serializerProvider, nestedConfigId, value, propertyName, null);
  }

  // Java版临时屏蔽
  // 动态属性数据序列化, 动态属性字段
  // 设计时生成代码如: this.WriteDynamicPropSetValue(writer, salesorder.DynamicProp1,
  // "DynamicProp1",
  // staticSerItems, serializer);
  protected final void writeDynamicPropSetValue(JsonGenerator writer,
      SerializerProvider serializerProvider,
      IDynamicPropSet data, String propertyName, IDynamicPropSerItem serItem) {
    Objects.requireNonNull(serItem);
    SerializerUtil.writeDynamicPropSetValue(writer, serializerProvider, data, propertyName, serItem);
  }

  // 动态属性变更集序列化, 动态属性字段
  // 设计时生成代码如: this.WriteDynamicPropSetChange(writer, (IChangeDetail)change,
  // "DynamicProp1",
  // staticSerItems, serializer);
  protected final void writeDynamicPropSetChange(JsonGenerator writer,
      SerializerProvider serializerProvider,
      IChangeDetail change, String propertyName, IDynamicPropSerItem serItem) {
    SerializerUtil.writeDynamicPropSetChange(writer, serializerProvider, change, propertyName, serItem);
  }

  // protected final void writeAssociation(JsonGenerator writer,
  // SerializerProvider serializerProvider, Object value, String propertyName,
  // Action<JsonGenerator, Object, JsonSerializer> infoWriter)
  // {
  // writer.WritePropertyName(StringUtils.toCamelCase(propertyName));
  //
  // if (value == null)
  // {
  // writer.WriteNull();
  // }
  // else
  // {
  // writer.WriteStartObject();
  // String idValue = null;
  // String idFieldName = null;
  // try
  // {
  // String name = value.getClass().getName();
  // if (name.endsWith("info", StringComparison.InvariantCultureIgnoreCase))
  // {
  // name = name.substring(0, name.length() - 4);
  //// C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing
  // in Java:
  // var propInfo = value.getClass().GetProperty(name);
  // if (propInfo.PropertyType == String.class)
  // {
  // idFieldName = propInfo.getName();
  // idValue = (String)propInfo.getValue(value);
  // }
  // }
  // }
  // catch (java.lang.Exception e)
  // {
  // }
  // if (idFieldName != null)
  // {
  // writeString(writer, idValue, idFieldName, serializer);
  // }
  // infoWriter(writer, value, serializer);
  // writer.WriteEndObject();
  // }
  // }

  public Object readModifyPropertyValue(JsonParser p, DeserializationContext ctxt,
      String propertyName,
      RefObject<Boolean> hasRead) {
    hasRead.argvalue = false;
    return null;
  }

  public Object readModifyPropertyValue(JsonParser p, DeserializationContext ctxt,
      RefObject<String> propertyName,
      RefObject<Boolean> hasRead) {
    hasRead.argvalue = false;
    return null;
  }

  public Object readModifyPropertyValue(JsonParser p, DeserializationContext ctxt,
                                        HashMap<String, Object> changeValues,
                                        RefObject<String> propertyName,
                                        RefObject<Boolean> hasRead,
                                        CefSerializeContext context) {
    hasRead.argvalue = false;
    return null;
  }

  public abstract boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt,
      ICefData data,
      String propertyName);

  protected final String readString(JsonParser p) {
    return SerializerUtil.readString(p);
  }

  /**
   * 读取整型数据
   *
   * @param p 值
   * @return 得到的整数值
   */
  protected final int readInt(JsonParser p) {
    return SerializerUtil.readInt(p);
  }

  /**
   * 读取小数数据
   *
   * @param p 值
   * @return 得到的小数值
   */
  protected final BigDecimal readDecimal(JsonParser p) {
    return SerializerUtil.readDecimal(p);
  }

  protected final BigDecimal getDecimalValue(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof Integer) {
      return new BigDecimal(value.toString());
    }
    if (value instanceof Double) {
      return BigDecimal.valueOf((Double) value);
    }
    if (value instanceof BigDecimal) {
      return (BigDecimal) value;
    }
    if (value instanceof Long) {
      return BigDecimal.valueOf((Long) value);
    }
    throw new RuntimeException("未识别的值类型：" + value.getClass().getTypeName());
  }

  /**
   * 读取布尔数据
   *
   * @param p 值
   * @return 得到的布尔值
   */
  protected final boolean readBool(JsonParser p) {
    return SerializerUtil.readBool(p);
  }

  /**
   * 读取日期数据
   *
   * @param p 值
   * @return 得到的日期值
   */
  protected final java.util.Date readDateTime(JsonParser p) {
    return readDateTime(p, getCefSerializeContext().getEnableStdTimeFormat());
  }

  protected final java.util.Date readStdDateTime(JsonParser p) {
    return SerializerUtil.readDateTime(p, true);
  }

  private java.util.Date readDateTime(JsonParser p, Boolean enableStdDateTime) {
    return SerializerUtil.readDateTime(p, enableStdDateTime);
  }

  /**
   * 读取二进制数据
   *
   * @param p 值
   * @return 得到的二进制数据值
   */
  protected final byte[] readBytes(JsonParser p) {
    return SerializerUtil.readBytes(p);
  }

  /**
   * 读取枚举数据
   *
   * @param p 值
   * @return 得到的枚举值
   */
  protected final <T extends Enum> T readEnum(JsonParser p, Class tClass) {
    return SerializerUtil.readEnum(p, tClass);
  }

  /**
   * 读取枚举数据
   *
   * @param p 值
   * @return 得到的枚举值
   */
  // TODO: 找机会删掉
  @Deprecated
  protected final <T> T readEnum(JsonParser p) {
    throw new UnsupportedOperationException();
  }

  // 关联带出字段反序列化readxxxAssociation用
  protected final <T extends Enum> T readEnum(Object p, Class tClass) {
    // Objects.requireNonNull(p);
    if (p == null) {
      return null;
    } else {
      return (T) Enum.valueOf(tClass, p.toString());
    }
  }

  /**
   * 读取关联数据
   *
   * @param p 值
   * @return 得到的关联值
   */
  protected final <T> T readAssociation(JsonParser p) {
    throw new UnsupportedOperationException();
    // Java版临时屏蔽
    // p.readValueAs(Class<T>);
    // return ((JToken)((value instanceof JToken) ? value : null)).<T>ToObject();
  }

  @Deprecated
  protected final <T> T readAssociation(Object value) {
    throw new UnsupportedOperationException();
    // Java版临时屏蔽
    // return Activator.<T>CreateInstance();
  }

  protected final java.util.HashMap<String, Object> readAssociationValue(JsonParser p,
      DeserializationContext ctxt) {

    java.util.HashMap<String, Object> transValue = new java.util.HashMap<String, Object>();
    if (p.getCurrentToken() == JsonToken.VALUE_NULL) {
      return transValue;
    }


    ObjectMapper mapper = JSONSerializer.getObjectMapper();
    // JsonNode value = mapper.valueToTree(p);
    JsonNode value = null;
    try {
      value = mapper.readValue(p, JsonNode.class);
    } catch (IOException e) {
      throw new RuntimeException("关联Json获取失败");
    }

    if (value != null) {
      Iterator<Map.Entry<String, JsonNode>> fields = value.fields();
      while (fields.hasNext()) {
        Map.Entry<String, JsonNode> item = fields.next();
        String propName = item.getKey();
        JsonNode changeValue = item.getValue();

        if (changeValue.isObject()) {
          transValue.put(propName, changeValue);
        } else {
          try {
            Object objValue = mapper.treeToValue(changeValue, Object.class);
            if(objValue != null) {
              transValue.put(propName, objValue);
            }
          } catch (JsonProcessingException e) {
            throw new RuntimeException(changeValue.toString() + "转换失败");
          }
        }
      }
    }
    // while (p.getCurrentToken() != JsonToken.END_OBJECT) {
    // String propName = null;
    // try {
    // propName = p.getValueAsString();
    // p.nextToken();
    //
    // Object changeValue = p.getCurrentValue();
    // if (p.getCurrentToken() == JsonToken.START_OBJECT) {
    //// changeValue = JToken.ReadFrom(reader);
    // readObject(p);
    // } else {
    // p.nextToken();
    // }
    // //reader.Read();
    // transValue.put(propName, changeValue);
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }

    return transValue;
  }

  private void readObject(JsonParser reader) throws IOException {
    if (reader.getCurrentToken() == JsonToken.START_OBJECT) {
      reader.nextToken();
    }
    while (reader.getCurrentToken() == JsonToken.FIELD_NAME) {
      reader.nextToken();
      reader.nextToken();
    }
  }

  @Deprecated
  protected final java.util.HashMap<String, Object> readAssociationValue(Object value) {
    return new java.util.HashMap<String, Object>();
  }

  protected final ICefData getUdtEntity(Object value, String configId) {
    if (value == null) {
      return null;
    }
    ICefValueObjManager val = (ICefValueObjManager) UdtManagerUtil.getUdtFactory()
        .createUdtManager(configId);
    return val.deserialize(((JsonNode) value).toString());
  }

  // 此方法不再使用, 兼容已生成dll暂时保留
  protected final <T> T readNestedValue(String nestedConfigId, Object value) {
    throw new UnsupportedOperationException();
    // Java版临时屏蔽
    // Object tempVar =
    // UdtManagerUtil.getUdtFactory().CreateManager(nestedConfigId);
    //// C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing
    // in Java:
    // var val = (ICefValueObjManager)((tempVar instanceof ICefValueObjManager) ?
    // tempVar : null);
    // return (T)val.CreateDataType();
  }

  protected final <T> T readNestedValue(String nestedConfigId, JsonParser p,
      DeserializationContext ctxt) {
    return readNestedValue(nestedConfigId, p, ctxt, null);
  }

  protected final <T> T readNestedValue(String nestedConfigId, JsonParser p,
      DeserializationContext ctxt,
      NestedSerializeContext nestedSerializeContext) {
    ICefValueObjManager val = (ICefValueObjManager) UdtManagerUtil.getUdtFactory()
        .createUdtManager(nestedConfigId);
    val.setNestedSerializeContext(nestedSerializeContext);
    try {
      return (T) val.deserialize(p.readValueAsTree().toString());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Deprecated
  protected final IChangeDetail readNestedChange(String nestedConfigId, Object value) {
    return new ValueObjModifyChangeDetail();
  }

  protected final IChangeDetail readNestedChange(String nestedConfigId, JsonParser p,
      DeserializationContext ctxt,
      NestedSerializeContext nestedSerializeContext) {
    try {
      return readNestedChange(nestedConfigId, (JsonNode) p.readValueAsTree(), ctxt,
          nestedSerializeContext);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected final IChangeDetail readNestedChange(String nestedConfigId, JsonParser p,
      DeserializationContext ctxt) {
    try {
      return readNestedChange(nestedConfigId, (JsonNode) p.readValueAsTree(), ctxt);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected final IChangeDetail readNestedChange(String nestedConfigId, JsonNode value,
      DeserializationContext ctxt,
      NestedSerializeContext nestedSerializeContext) {
    Object tempVar = UdtManagerUtil.getUdtFactory().createUdtManager(nestedConfigId);
    ICefValueObjManager val = (ICefValueObjManager) ((tempVar instanceof ICefValueObjManager)
        ? tempVar : null);
    val.setNestedSerializeContext(nestedSerializeContext);
    return val.deserializeChange(value.toString());
  }

  protected final IChangeDetail readNestedChange(String nestedConfigId, JsonNode value,
      DeserializationContext ctxt) {
    return readNestedChange(nestedConfigId, value, ctxt, null);
  }

  protected JsonParser getEnrichedValue(JsonNode value, String key) {
    Objects.requireNonNull(key);
    Objects.requireNonNull(value);
    value = value.get("ChangeInfo");
    if (value == null || !value.fields().hasNext()) {
      return null;
    }
    value = value.fields().next().getValue();
    Iterator<Map.Entry<String, JsonNode>> iterator = value.fields();
    while (iterator.hasNext()) {
      Map.Entry<String, JsonNode> next = iterator.next();
      if (key.equals(next.getKey()) || StringUtils.toCamelCase(key).equals(next.getKey())) {
        JsonParser parser = next.getValue().traverse(new ObjectMapper());
        try {
          parser.nextToken();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        return parser;
      }
    }
    return null;
  }

  // 动态属性data反序列化, 动态属性字段
  /*
   * 设计时生成代码如: case "DynamicProp1": ReadDynamicPropInfo(reader,
   * salesOrder.DynamicProp1, "DynamicProp1", staticSerItems, serializer); return
   * true;
   */
  protected void readDynamicPropSetValue(IDynamicPropSet data, String dynPropSetPropName,
      IDynamicPropSerItem serItem,
      JsonParser reader, DeserializationContext ctx) {
    SerializerUtil.readDynamicPropSetValue(data, dynPropSetPropName, serItem, reader, ctx);
  }

  // 动态属性变更集反序列化, 动态属性字段
  /*
   * 设计时生成代码如: case "DynamicProp1": hasRead = true; return
   * ReadDynamicPropSetChange(propertyValue, propName, staticSerItems,
   * serializer);
   */
  protected IChangeDetail readDynamicPropSetChange(JsonParser reader, DeserializationContext ctx,
      String dynPropSetPropName, IDynamicPropSerItem serItem) {
    return SerializerUtil.readDynamicPropSetChange(reader, ctx, dynPropSetPropName, serItem);
  }

  protected boolean isPropertiesEquals(String proName1, String propName2) {
    if (proName1 == null || proName1.equals("") || propName2 == null || propName2.equals("")) {
      throw new RuntimeException("属性不能为空。");
    }
    return proName1.equalsIgnoreCase(propName2);
  }

  private CefSerializeContext serializeContext;

  protected CefSerializeContext getCefSerializeContext() {
    if (this.serializeContext == null) {
      this.serializeContext = new CefSerializeContext();
    }
    return this.serializeContext;
  }

  public void setCefSerializeContext(CefSerializeContext context) {
    this.serializeContext = context;
  }
}
