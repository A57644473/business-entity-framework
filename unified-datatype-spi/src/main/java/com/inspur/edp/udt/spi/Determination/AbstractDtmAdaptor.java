/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.spi.Determination;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.udt.api.Determination.IDeterminationContext;

public abstract class AbstractDtmAdaptor implements IDetermination
{
	public abstract String getName();

	public abstract boolean CanExecute(IChangeDetail change);

	public abstract void execute(IDeterminationContext context, IChangeDetail change);

	public	void execute(ICefDeterminationContext context, IChangeDetail change) {
	execute((IDeterminationContext) context, change);
}
}
