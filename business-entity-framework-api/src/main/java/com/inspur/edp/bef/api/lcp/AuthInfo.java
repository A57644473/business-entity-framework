/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

/** 
 权限信息类
 
*/
public class AuthInfo
{
	private String privateExtType;
	public final String getExtType()
	{
		return privateExtType;
	}
	public final void setExtType(String value)
	{
		privateExtType = value;
	}
	private String privateExtend1;
	public final String getExtend1()
	{
		return privateExtend1;
	}
	public final void setExtend1(String value)
	{
		privateExtend1 = value;
	}
	private String privateExtend2;
	public final String getExtend2()
	{
		return privateExtend2;
	}
	public final void setExtend2(String value)
	{
		privateExtend2 = value;
	}
	private String privateExtend3;
	public final String getExtend3()
	{
		return privateExtend3;
	}
	public final void setExtend3(String value)
	{
		privateExtend3 = value;
	}
	private String privateExtend4;
	public final String getExtend4()
	{
		return privateExtend4;
	}
	public final void setExtend4(String value)
	{
		privateExtend4 = value;
	}
	private String privateExtend5;
	public final String getExtend5()
	{
		return privateExtend5;
	}
	public final void setExtend5(String value)
	{
		privateExtend5 = value;
	}
	private String sourceType;
	public final String getSourceType() {
		return sourceType;
	}
	public final void setSourceType(String value)
	{
		sourceType = value;
	}
	public final void SourceType(String value)
	{
		sourceType = value;
	}
}
