//package com.inspur.edp.bef.api.authority;
//
//import com.inspur.edp.bef.api.com.inspur.edp.bef.core.action.com.inspur.edp.bef.core.assembler.IBEActionAssembler;
//import com.inspur.edp.cef.api.authority.AuthorityInfo;
//import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
//import com.inspur.edp.cef.entity.entity.IEntityData;
//
//public final class AuthUtil
//{
//	public static boolean DemandDataPermission(IEntityData data, java.util.ArrayList<Inspur.Gsp.Bef.Api.Authority.DataPermissionContext> ctxList)
//	{
////		DataValidator.CheckForNullReference(ctxList, "ctxList");
////		DataValidator.CheckForNullReference(data, "data");
//
//		var authSvr = ServiceManager.<IDataPermission>GetService();
//		var racContextList = ctxList.Select(befCtx => befCtx.ToRacContext(data)).ToList();
//		return Task.Run(() => authSvr.IsHasDataPermissionAsync(racContextList)).Result;
//	}
//
//	public static void DemandFuncPermission(String funcOpId)
//	{
//		DataValidator.CheckForNullReference(funcOpId, "funcOpId");
//
//		var permissionRez = ServiceManager.<IPermission>GetService().IsPrincipalHasOp(funcOpId);
//		if (!permissionRez)
//		{
//			throw new Inspur.Gsp.Bef.Api.Exceptions.BefFuncPermissionDeniedException();
//		}
//	}
//
//	public static java.util.ArrayList<AuthorityInfo> GetQueryFilters(String authorizationId, String authOpId, java.util.ArrayList<String> authFields, java.util.ArrayList<String> beFields)
//	{
//		DataValidator.CheckForEmptyString(authorizationId, "authorizationId");
//		DataValidator.CheckForEmptyString(authOpId, "authOpId");
//		DataValidator.CheckForNullReference(authFields, "authFields");
//		DataValidator.CheckForNullReference(beFields, "beFields");
//
//		var authSvr = ServiceManager.<IDataPermission>GetService();
//		var svrRez = Task.Run(() => authSvr.GetQueryFiltersAsync(authorizationId, authFields, authOpId)).Result;
//		AuthorityInfo tempVar = new AuthorityInfo();
//		tempVar.setSourceFieldName(item.DataAuthResultDefColumnName);
//		tempVar.setAuthoritySql(item.DataAuthResultFilter);
//		tempVar.setFieldName(beFields.get(index));
//		return svrRez.Select((item, index) => tempVar).ToList();
//	}
//
//	//public static AuthorityInfo GetQueryFilter(string authorizationId, string authField, string authOpId)
//	//{
//	//    DataValidator.CheckForEmptyString(authorizationId, "authorizationId");
//	//    DataValidator.CheckForEmptyString(authOpId, "authOpId");
//	//    DataValidator.CheckForEmptyString(authField, "authField");
//
//	//    var authSvr = ServiceManager.GetService<IDataPermission>();
//	//    var svrRez = Task.Run(() => authSvr.GetQueryFilterAsync(authorizationId, authField, authOpId)).Result;
//	//    return new AuthorityInfo(svrRez);
//	//}
//
//	/**
//	 自定义be动作执行前执行, 判断数据权限
//
//	 @param beCtx
//	 @param ass
//	*/
//	public static void DemandDataPermission(Inspur.Gsp.Bef.Api.BE.IBEContext beCtx, IBEActionAssembler ass)
//	{
//		DataValidator.CheckForNullReference(beCtx, "beCtx");
//		DataValidator.CheckForNullReference(ass, "ass");
//
//		if (beCtx.getCurrentData() == null)
//		{
//			try
//			{
//				Inspur.Gsp.Bef.Api.Parameter.RetrieveParam tempVar = new Inspur.Gsp.Bef.Api.Parameter.RetrieveParam();
//				tempVar.setNeedLock(false);
//				beCtx.getBizEntity().Retrieve(tempVar);
//			}
//			catch (Inspur.Gsp.Bef.Api.Exceptions.BefDataPermissionDeniedException e)
//			{
//			}
//			if (beCtx.getCurrentData() == null)
//			{
//				return;
//			}
//		}
//		if (!ass.DemandDataPermission(beCtx.getCurrentData()))
//		{
//			throw new Inspur.Gsp.Bef.Api.Exceptions.BefDataPermissionDeniedException();
//		}
//	}
//}